# The Blockchain

https://web3.coach/

# Genesis

Hyacinth Dupas is owner of the Twisted Rifters Travel Club. 
She decides that the club ledger will be created with 1M tokens in it.
The token will be called miles, and you should be able to buy things with it.

Maps: 2 miles
Mug: 5 miles
Non branded walking shoes: 100 miles
Coffee latte: 1 miles
Subscription: 10 miles/month

Plus a 95 miles / day for Hyacinth (just because).

The blockchain is a database

# States

Hyacinth opened her club 10 days ago. No visits but she's keeping her spirit high.
She writes down her increase in Salary, 10 days means 950 miles more.

Aimee Kowalik is the first member! 
Aimee buys 3654 miles in advance

```
H: -3654
A: +3654
```

She uses them to get a Mug with a latte in it.
```
H: +6
A: -6
```

The genesis balances indicate the original blockchain state and never changes. 
State changes are transactions.

